package core;

/**
 * The core functionalities of a Connect 4 game. Can be implemented with a console ui or other ui.
 * 
 * @author Spencer Brown
 * @version 1.4.0
 * 
 */
public class Connect4 {
	/** spots holds all of the move data in a 2D array. */
	private char[][] spots = new char[6][7];
	
	/** The current player's character (either 'X' or 'O'). */
	private char currentPlayer;
	
	/**
	 * Main constructor. Sets the current player to 'X' and sets all spots
	 * with an space string.
	 */
	public Connect4() {
		this.currentPlayer = 'X';
		this.resetSpots();
	}
	
	/**
	 * Initializes all 42 spots in the 2D spots array to a space (' ');
	 */
	public void resetSpots() {
		for(int x = 0; x < 6; x++) 
			for(int y = 0; y < 7; y++)
				this.spots[x][y] = ' ';
	}
	
	/**
	 * Returns this spots[][].
	 * @return char[][]
	 */
	public char[][] getSpots() {
		return this.spots;
	}
	
	/**
	 * Adds a coin to the inputted col in the spots 2D array if there is an 
	 * opening at the top of the column.
	 * @param col Column number (zero indexed)
	 */
	public void addCoin(int col) {
		// Check for the next available spot in the col and then add it.
		for(int row = spots.length - 1; row >= 0; row--) {
			if(spots[row][col] == ' ') {
				spots[row][col] = getCurrentPlayer();
				return;
			}
		}
	}
	
	/**
	 * Determines if the given column is available for more checkers. Returns
	 * true if available, false otherwise.
	 * @param col Column number (zero indexed)
	 * @return boolean
	 */
	public boolean colAvailable(int col) {
		try {
			if(spots[0][col] == ' ') {
				return true;
			} else {
				return false;
			}
		}
		// If there is an array out of bounds exception, then the column is not available.
		catch(ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}
	
	/**
	 * Determines whether any spots are available on the board using the spots 2D
	 * array. Returns true if there are spots available, false otherwise.
	 * @return boolean
	 */
	public boolean spotsAvailable() {
		for(int x = 0; x < spots[0].length; x++) {
			if(spots[0][x] == ' ' )
				return true;
		}
		return false;
	}
	
	/**
	 * Returns the current player's character.
	 * @return char
	 */
	public char getCurrentPlayer() {
		return this.currentPlayer;
	}
	
	/**
	 * Updates the current player to the next player. If X then O. If O then X.
	 */
	public void nextPlayer() {
		this.currentPlayer = this.currentPlayer == 'X' ? 'O' : 'X';
	}
	
	/**
	 * Checks if 4 are in a row in the spots 2D array. Checks for columns, rows, and diagonals.
	 * If 4 in a row are found then returns true, false if none found.
	 * @return boolean
	 */
	public boolean are4InARow() {

		// Get player's char
		char player = this.getCurrentPlayer();

		// Check every row
		for(int row = 0; row < spots.length; row++) {
			int count = 0;
			for(int col = 0; col < spots[row].length; col++) {
				if(spots[row][col] == player) {
					++count;
					if(count == 4)
						return true;
				}
				else
					count = 0;
			}
		}

		// Check every column
		for(int col = 0; col < spots[0].length; col++){
			int count = 0;
			for(int row = 0; row < spots.length; row++){
				if(spots[row][col] == player) {
					++count;
					if(count == 4)
						return true;
				}
				else
					count = 0;
			}
		}
		
		// Check first half (bottom left going down and to the right)
		for(int row = 2; row >= 0; row--) {
			int count = 0;
			int tempCol = 0;
			for(int tempRow = row; tempRow < spots.length; tempRow++) {
				if(spots[tempRow][tempCol] == player) {
					++count;
					if(count == 4)
						return true;
				}
				else
					count = 0;
				tempCol++;
			}
		}
		
		// Check second half (middle down-right-diagonal to top right section)
		for(int col = 1; col <= 3; col++) {
			int count = 0;
			int tempRow = 0;
			for(int tempCol = col; tempCol < spots[0].length; tempCol++) {
				//System.out.print(tempRow + ":" + tempCol + " = ");
				//System.out.println(spots[tempRow][tempCol]);
				if(spots[tempRow][tempCol] == player) {
					++count;
					if(count == 4)
						return true;
				}
				else
					count = 0;
				tempRow++;
			}		
		}
		
		// Check first half (bottom right going up and to the left)
		for(int row = 2; row >= 0; row--) {
			int count = 0;
			int tempCol = 6;
			for(int tempRow = row; tempRow < spots.length; tempRow++) {
				if(spots[tempRow][tempCol] == player) {
					++count;
					if(count == 4)
						return true;
				}
				else
					count = 0;
				tempCol--;
			}
		}
		
		// Check second half (middle down-left-diagonal to top left section)
		for(int col = 5; col > 2; col--) {
			int count = 0;
			int tempRow = 0;
			for(int tempCol = col; tempCol >= 0; tempCol--) {
				if(spots[tempRow][tempCol] == player) {
					++count;
					if(count == 4)
						return true;
				}
				else
					count = 0;
				tempRow++;
			}
		} 
		
		// If it got this far and found no 4 in a row return false.
		return false;
	}
	
}
