package core;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Methods to produce a good guess column for the computer to play on.
 * @author Spencer Brown
 * @version 1.1.0
 */
public class Connect4ComputerPlayer {
    /**
     * Make a choice that is based off the highest column. Have option to delay the response to make it feel more
     * human like
     * @param spots char[][]
     * @param delay boolean
     * @return int
     */
    public static int makeChoice(char[][] spots, boolean delay) {
        int busyCol = findBusyColumn(spots);
        int choice = getWeightedRandom(busyCol, delay);
        return choice;
    }

    /**
     * Find the column that is the highest (the first one if there is a tie).
     * @param spots char[][]
     * @return int
     */
    private static int findBusyColumn(char[][] spots) {
        for(int row = 0; row < 6; row++) {
            for(int col = 0; col < 7; col++) {
                // If there is either an 'X' or 'O' return the column.
                if(spots[row][col] == 'X' || spots[row][col] == 'O') {
                    return col;
                }
            }
        }
        return 3; // Defaults to middle of board.
    }

    /**
     * Create a random int that is normalized around the input int.
     * @param highest int
     * @param delay boolean
     * @return int
     */
    private static int getWeightedRandom(int highest, boolean delay) {
        // Delay the response time to make feel more like a human.
        try {
            if(delay){
                TimeUnit.SECONDS.sleep(1);
                System.out.print(".");
                TimeUnit.SECONDS.sleep(1);
                System.out.print(".");
                TimeUnit.SECONDS.sleep(1);
                System.out.print(".");
            }
        } catch (InterruptedException e) {
            // Catches exception.
        } finally {
            // Make Random object.
            Random rand = new Random();
            // Attempt to normally distribute around the highest column with median of highest
            // and standard deviation of 1.
            return (int) Math.round(rand.nextGaussian() * 1.2) + highest;
        }
    }
}
