package ui;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The JavaFX driver class that starts the GUI. The Controller class handles user events and game logic.
 * Relies on game.fxml and settings.fxml files in this same directory.
 * @author Spencer Brown
 * @version 1.0.0
 */
public class Connect4GUI extends Application {

    /** The stage to put scenes on. */
    Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception{
        // Think of primaryStage like a window in web dev.
        window = primaryStage;

        // The parent scene.
        Parent rootStart = FXMLLoader.load(getClass().getResource("settings.fxml"));

        // The window title.
        window.setTitle("Connect 4 - WebSpence Studios");

        // Add the scene to the window (stage).
        window.setScene(new Scene(rootStart, 600, 400));

        // Actually show it!
        window.show();
    }

    /**
     * Initiator method to start the GUI.
     */
    public void playGame() {
        // Launch the GUI!
        try {
            launch();
        } catch (Exception e) {
            System.out.println("There was a problem starting the GUI.");
        }
    }
}
