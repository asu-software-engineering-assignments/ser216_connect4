package ui;
import core.Connect4;
import core.Connect4ComputerPlayer;

import java.util.Scanner;


/**
 * Terminal UI class to test and play the Connect4 game.
 * @author Spencer Brown
 * @version 1.4.0
 */
public class Connect4TextConsole {

	/** The Connect4 object to be used in the UI. */
	static Connect4 game = new Connect4();


	/** Whether the game is against the computer or not. */
	static boolean againstComputer = false;


	/**
	 * Default constructor.
	 */
	public Connect4TextConsole() {}


	/**
	 * Get the user's column choice for placing the checker. Validates that
	 * input is an integer and is a number 1 - 7. Will continue to prompt user
	 * for number until a valid integer of 1 - 7 is entered. Returns the column
	 * number as an integer.
	 * @param scan Scanner object
	 * @return int
	 */
	public static int getColChoice(Scanner scan) {
		int col = -1;
		do {
			while(!scan.hasNextInt()) {
				scan.nextLine();
				System.out.println("Please enter valid number 1 - 7.");
			}
			col = scan.nextInt(); // TODO: Error checking.
			scan.nextLine();
			
			// Give error message if input is not 1 - 7.
			if(col > 7 || col < 1)
				System.out.println("Please enter a number 1 - 7.");
			
		} while(col > 7 || col < 1);
		
		// Return the index-0 version of the inputted column.
		return col - 1;
	}


	/**
	 * Get the user's choice to whether they want to play against another player or the computer. Updates the static
	 * againstComputer variable.
	 * @param scan Scanner
	 */

	public static void getGameType(Scanner scan) {
		char choice = ' ';
		// Try to get the first character.
		try {
			choice = scan.nextLine().charAt(0);

			// Convert to lower case.
			choice = Character.toLowerCase(choice);
		}
		// Catch any exceptions and try again if there is an exception.
		catch (Exception e) {
			System.out.println("Enter ‘P’ if you want to play against another player; enter ‘C’ to play against computer.");
			getGameType(scan);
		}

		// Check the character and update whether this is a game vs human or computer.
		// If neither, then try again.
		if(choice == 'p') {
			againstComputer = false;
		} else if(choice == 'c') {
			System.out.println("Start game against computer.");
			againstComputer = true;
		} else {
			System.out.println("Enter ‘P’ if you want to play against another player; enter ‘C’ to play against computer.");
			getGameType(scan);
		}
	}
	

	/**
	 * Displays the game board by joining all spot values with a "|" String
	 * for each character in each row for all rows.
	 * @param spots This spots[][] 2D array
	 */

	public static void displayBoard(char[][] spots) {
		String board = "|1|2|3|4|5|6|7|\n";
		for(int row = 0; row < spots.length; row++) {
			for(int col = 0; col < spots[row].length; col++) {
				board += "|" + String.valueOf(spots[row][col]);
			}
			board += "|\n";
		}
		System.out.print(board);
	}


	/**
	 * Get the next choice, regardless of whether the user is a computer or human.
	 * @param scan Scanner
	 * @return int
	 */

	public static int getNextChoice(Scanner scan) {
		// The selected column number.
		int col = 0;

		// If against the computer check if player is 'X'. Otherwise, play as normal (going back and forth).
		if(againstComputer) {
			// Get player's col choice from terminal if human ('X').
			if(game.getCurrentPlayer() == 'X') {
				col = getColChoice(scan);
			} else {
				// Get the computer's column choice until the choice is available.
				do {
					col = Connect4ComputerPlayer.makeChoice(game.getSpots(), true);
				}while(!game.colAvailable(col));
				System.out.println();
			}
		} else {
			col = getColChoice(scan);
		}

		return col;
	}

	/**
	 * Get the user's choice of playing in terminal or playing on GUI and then start the
	 * user's choice.
	 * @param scan Scanner
	 */
	private static void getInterfaceType(Scanner scan) {
		//Choice variable.
		char choice = ' ';

		// Ask for user choice while choice is invalid.
		System.out.println("Would you like to play using the GUI? (y/n)");

		// Try to get the first character.
		try {
			choice = scan.nextLine().charAt(0);

			// Convert to lower case.
			choice = Character.toLowerCase(choice);
		}
		// Catch any exceptions and try again if there is an exception.
		catch (Exception e) {
			System.out.println("Invalid response, please enter either 'y' or 'n'.");
			getInterfaceType(scan);
		}

		// If yes, start GUI.
		if(choice == 'y'){
			Connect4GUI GUIGame = new Connect4GUI();
			GUIGame.playGame();
		}
		// Else if no, start terminal.
		else if(choice == 'n') {
			playTerminalVersion(scan);
		}
		// Else, ask again until their is a valid choice.
		else {
			System.out.println("Invalid response, please enter either 'y' or 'n'.");
			getInterfaceType(scan);
		}
	}

	/**
	 * Play the game in the terminal.
	 * @param scan Scanner
	 */
	public static void playTerminalVersion(Scanner scan) {

		boolean started = false;

		displayBoard(game.getSpots());

		// Do the game while there are spots available.
		do {
			// Message about the beginning of game if it is the first time.
			if(started) {
				System.out.println("");
			}
			else {
				System.out.println("Begin Game. Enter ‘P’ if you want to play against another player; enter ‘C’ to play against computer.");
				getGameType(scan);
				started = true;
			}

			// Prompt user for column. The style depends on whether it is against the computer.
			if(againstComputer) {
				String name = game.getCurrentPlayer() == 'X' ? "It is your turn." : "Computer, your turn.";
				System.out.println(name + " Choose a column number from 1-7.");
			} else {
				System.out.println("Player" + game.getCurrentPlayer() + " - your turn. Choose a column number from 1-7");
			}
			boolean colAvailable = false;

			do {
				// Get player's choice.
				int col = getNextChoice(scan);

				colAvailable = game.colAvailable(col);
				if(colAvailable) {
					// Add player char to top spot in col.
					game.addCoin(col);
				}
				else
					// Message player of bad choice
					System.out.println("Sorry this column is full. Please choose another column.");
			} while(!colAvailable);

			displayBoard(game.getSpots());

			//check for are4InARow
			if(game.are4InARow()) {
				if(againstComputer) {
					String name = game.getCurrentPlayer() == 'X' ? "You" : "The Computer";
					System.out.println(name + " Won the Game");
				} else {
					System.out.println("Player " + game.getCurrentPlayer() + " Won the Game");
				}

				return;
			}

			// Advance to next player's turn (change to next player).
			game.nextPlayer();

		} while (game.spotsAvailable());

		System.out.println("Draw!");

		return;
	}
	

	/**
	 * Start the program with a Scanner object.
	 * @param args Standard args
	 */
	public static void main(String[] args) {
		// Scanner object to be used in this and other methods.
		Scanner scan = new Scanner(System.in);

		// Does user want terminal or GUI?
		// Start the user's choice.
		getInterfaceType(scan);
		
		scan.close();
	}

}
