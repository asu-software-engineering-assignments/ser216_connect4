package ui;

import core.Connect4;
import core.Connect4ComputerPlayer;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import static ui.CheckerValue.*;

/**
 * The controller class handles all user interactions with the game board and the settings page. There is some game
 * logic here.
 * @author Spencer Brown
 * @version 1.0.0
 */
public class Controller {

    /** The Connect4 object to be used in the UI. */
    private static Connect4 game = new Connect4();

    /** Hold all spots (Circle objects) in one array. */
    private static Spot[][] spots = new Spot[6][7];

    /** Current player. */
    private static CheckerValue currentPlayer = PLAYER1;

    /** Game state variable. */
    private static boolean gameOver = false;

    /** The winner of the game. */
    private static String winner = "Draw";

    /** Whether the user is playing against a computer or not. */
    private static boolean computerMode = false;

    /** Is the computer thinking? */
    private static boolean computerThinking = false;

    // The label where the game messages will appear.
    public Label messageLabel;

    // The label that describes the user's choice.
    public Label humanVsMachineLabel;

    // The gameboard object.
    public GridPane gameBoard;

    // All the Circle objects that are used as the spots that users can use.
    public Circle spot_0_0;
    public Circle spot_0_1;
    public Circle spot_0_2;
    public Circle spot_0_3;
    public Circle spot_0_4;
    public Circle spot_0_5;
    public Circle spot_0_6;
    public Circle spot_1_0;
    public Circle spot_1_1;
    public Circle spot_1_2;
    public Circle spot_1_3;
    public Circle spot_1_4;
    public Circle spot_1_5;
    public Circle spot_1_6;
    public Circle spot_2_0;
    public Circle spot_2_1;
    public Circle spot_2_2;
    public Circle spot_2_3;
    public Circle spot_2_4;
    public Circle spot_2_5;
    public Circle spot_2_6;
    public Circle spot_3_0;
    public Circle spot_3_1;
    public Circle spot_3_2;
    public Circle spot_3_3;
    public Circle spot_3_4;
    public Circle spot_3_5;
    public Circle spot_3_6;
    public Circle spot_4_0;
    public Circle spot_4_1;
    public Circle spot_4_2;
    public Circle spot_4_3;
    public Circle spot_4_4;
    public Circle spot_4_5;
    public Circle spot_4_6;
    public Circle spot_5_0;
    public Circle spot_5_1;
    public Circle spot_5_2;
    public Circle spot_5_3;
    public Circle spot_5_4;
    public Circle spot_5_5;
    public Circle spot_5_6;

    /**
     * Organizes all spots into one array for easy access.
     */
    private void setSpots() {
        spots[ 0 ][ 0 ] =  new Spot(spot_0_0, "spot_0_0");
        spots[ 0 ][ 1 ] =  new Spot(spot_0_1, "spot_0_1");
        spots[ 0 ][ 2 ] =  new Spot(spot_0_2, "spot_0_2");
        spots[ 0 ][ 3 ] =  new Spot(spot_0_3, "spot_0_3");
        spots[ 0 ][ 4 ] =  new Spot(spot_0_4, "spot_0_4");
        spots[ 0 ][ 5 ] =  new Spot(spot_0_5, "spot_0_5");
        spots[ 0 ][ 6 ] =  new Spot(spot_0_6, "spot_0_6");
        spots[ 1 ][ 0 ] =  new Spot(spot_1_0, "spot_1_0");
        spots[ 1 ][ 1 ] =  new Spot(spot_1_1, "spot_1_1");
        spots[ 1 ][ 2 ] =  new Spot(spot_1_2, "spot_1_2");
        spots[ 1 ][ 3 ] =  new Spot(spot_1_3, "spot_1_3");
        spots[ 1 ][ 4 ] =  new Spot(spot_1_4, "spot_1_4");
        spots[ 1 ][ 5 ] =  new Spot(spot_1_5, "spot_1_5");
        spots[ 1 ][ 6 ] =  new Spot(spot_1_6, "spot_1_6");
        spots[ 2 ][ 0 ] =  new Spot(spot_2_0, "spot_2_0");
        spots[ 2 ][ 1 ] =  new Spot(spot_2_1, "spot_2_1");
        spots[ 2 ][ 2 ] =  new Spot(spot_2_2, "spot_2_2");
        spots[ 2 ][ 3 ] =  new Spot(spot_2_3, "spot_2_3");
        spots[ 2 ][ 4 ] =  new Spot(spot_2_4, "spot_2_4");
        spots[ 2 ][ 5 ] =  new Spot(spot_2_5, "spot_2_5");
        spots[ 2 ][ 6 ] =  new Spot(spot_2_6, "spot_2_6");
        spots[ 3 ][ 0 ] =  new Spot(spot_3_0, "spot_3_0");
        spots[ 3 ][ 1 ] =  new Spot(spot_3_1, "spot_3_1");
        spots[ 3 ][ 2 ] =  new Spot(spot_3_2, "spot_3_2");
        spots[ 3 ][ 3 ] =  new Spot(spot_3_3, "spot_3_3");
        spots[ 3 ][ 4 ] =  new Spot(spot_3_4, "spot_3_4");
        spots[ 3 ][ 5 ] =  new Spot(spot_3_5, "spot_3_5");
        spots[ 3 ][ 6 ] =  new Spot(spot_3_6, "spot_3_6");
        spots[ 4 ][ 0 ] =  new Spot(spot_4_0, "spot_4_0");
        spots[ 4 ][ 1 ] =  new Spot(spot_4_1, "spot_4_1");
        spots[ 4 ][ 2 ] =  new Spot(spot_4_2, "spot_4_2");
        spots[ 4 ][ 3 ] =  new Spot(spot_4_3, "spot_4_3");
        spots[ 4 ][ 4 ] =  new Spot(spot_4_4, "spot_4_4");
        spots[ 4 ][ 5 ] =  new Spot(spot_4_5, "spot_4_5");
        spots[ 4 ][ 6 ] =  new Spot(spot_4_6, "spot_4_6");
        spots[ 5 ][ 0 ] =  new Spot(spot_5_0, "spot_5_0");
        spots[ 5 ][ 1 ] =  new Spot(spot_5_1, "spot_5_1");
        spots[ 5 ][ 2 ] =  new Spot(spot_5_2, "spot_5_2");
        spots[ 5 ][ 3 ] =  new Spot(spot_5_3, "spot_5_3");
        spots[ 5 ][ 4 ] =  new Spot(spot_5_4, "spot_5_4");
        spots[ 5 ][ 5 ] =  new Spot(spot_5_5, "spot_5_5");
        spots[ 5 ][ 6 ] =  new Spot(spot_5_6, "spot_5_6");
    }

    /**
     * Handles when user selects playing against the computer.
     * @param actionEvent ActionEvent
     * @throws Exception if something goes wrong while loading
     */
    public void selectedComputer(ActionEvent actionEvent) throws Exception {
        System.out.println("Computer Has Been Chosen!");
        computerMode = true;
        switchScenes(actionEvent);
    }

    /**
     * Handles when user selects playing against another human.
     * @param actionEvent ActionEvent
     * @throws Exception if something goes wrong while loading
     */
    public void selectedHuman(ActionEvent actionEvent) throws Exception {
        System.out.println("Human Has Been Chosen!");
        computerMode = false;
        switchScenes(actionEvent);
    }

    /**
     * Switches the scene to the game board. Also, sets the static spots variable.
     * @param actionEvent ActionEvent
     * @throws Exception if something goes wrong while loading
     */
    private void switchScenes(ActionEvent actionEvent) throws Exception {
        setSpots();

        // Get the main stage.
        Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();

        // Loads the game.fxml.
        Parent rootGame = FXMLLoader.load(getClass().getResource("game.fxml"));

        // Adds the scene to the stage.
        window.setScene(new Scene(rootGame, 1000, 800));
    }

    /**
     * Handles when user clicks on a spot.
     * @param e MouseEvent
     */
    public void mouseClicked(MouseEvent e) {
        // Get the source of the click (the spot node).
        Node source = (Node)e.getSource();

        // Get the column and row of the spot in the GridPane.
        Integer colIndex = GridPane.getColumnIndex(source);
        Integer rowIndex = GridPane.getRowIndex(source);

        // For the first row and column, need to set to 0 when it is not explicitly defined.
        if(colIndex == null) colIndex = 0;
        if(rowIndex == null) rowIndex = 0;
        System.out.printf("Mouse entered cell [%d, %d]%n", colIndex, rowIndex);

        // Handle the user's choice.
        handleChoice(colIndex);
    }

    /**
     * Handle the user's choice of column.
     * @param col int
     */
    private void handleChoice(int col){
        // If game over, reiterate that the winner won or there was a draw and exit.
        if(gameOver){
            if(winner.compareTo("Draw") == 0){
                messageLabel.setText("Draw!");
            } else {
                if(computerMode && winner.compareTo("Player Two") == 0){
                    messageLabel.setText("The computer won!");
                } else {
                    messageLabel.setText(winner + " won!");
                }
            }
            return;
        }

        // Do not process click if the computer is playing and thinking.
        if(computerMode && computerThinking) {
            return;
        }

        // If column available add coin and update user.
        if(game.colAvailable(col)) {
            // Process the user's choice.
            try{
                processUserChoice(col);
            } catch (Exception e) {
                System.out.println("There was a problem processing the user's choice. Please restart the game.");
            }

            // If playing against computer, let the computer play a turn.
            if(computerMode && !gameOver) {
                System.out.println("Computer's turn!");
                computersTurn();
            }
        }
        // Else, report an error.
        else {
            reportError("Column Full!");
        }
    }

    /**
     * Process the user's column choice by updating the game board object and GUI.
     * @param col int
     */
    private void processUserChoice(int col) {
        // Add the coin.
        game.addCoin(col);

        // For testing.
        displayBoard(game.getSpots());

        // Check for win.
        checkForWin();

        // Set next player.
        nextPlayer();

        // Display the updated board on the GUI.
        showUpdatedBoard(gameBoard);
    }

    /**
     * Perform the computer's turn and update it in the game board object and GUI.
     */
    private void computersTurn() {
        int col;

        // Let the computer think and try picking a column.
        computerThinking = true;

        // Get the computer's column choice until the choice is available.
        do {
            col = Connect4ComputerPlayer.makeChoice(game.getSpots(), false);
        }while(!game.colAvailable(col));

        // Once a legit column is found add it to the board.
        processUserChoice(col);

        // Computer is done thinking and the human user can now click on the spots.
        computerThinking = false;
    }

    /**
     * Check if there is a win and set the winner if there is a win.
     */
    private void checkForWin() {
        // If there is four in a row.
        if(game.are4InARow()){
            String player = currentPlayer == PLAYER1 ? "Player One" : "Player Two";
            winner = player;
            messageLabel.setText(player + ", You One!");
            gameOver = true;
        } else if(!game.spotsAvailable()) {
            messageLabel.setText("Draw!");
            gameOver = true;
        }
    }

    /**
     * Display the board in the terminal for testing.
     * @param spots char[][]
     */
    private static void displayBoard(char[][] spots) {
        StringBuilder board = new StringBuilder();
        for (char[] spot : spots) {
            for (char c : spot)
                board.append("|").append(c);
            board.append("|\n");
        }
        System.out.print(board);
    }

    /**
     * Report an error by prepending the message to the beginning of the messageLabel.
     * @param error String
     */
    private void reportError(String error) {
        String message = messageLabel.getText();
        message = error + " - " + message;
        messageLabel.setText(message);
    }

    /**
     * Set the next player and update the messageLabel to prompt the appropriate user.
     */
    private void nextPlayer() {
        game.nextPlayer();
        currentPlayer = (currentPlayer == PLAYER1) ? PLAYER2 : PLAYER1;
        String player = (currentPlayer == PLAYER1) ? "one" : "two";
        messageLabel.setText("Player " + player + ", your turn!");
    }

    /**
     * Display the updated game board on the GUI.
     * @param gameBoard GridPane
     */
    private void showUpdatedBoard(GridPane gameBoard) {
        // Convert the base spots ('X', 'O', and ' ') to objects.
        baseSpotsToObjects(game.getSpots());

        // Loop through each spot and update it on the board.
        for(int row = 0; row < spots.length; row++) {
            for(int col = 0; col < spots[row].length; col++) {
                Spot spot = spots[row][col];
                Circle circle = (Circle) gameBoard.lookup("#" + spot.id);
                switch(spot.value) {
                    case EMPTY:
                        circle.setFill(Color.WHITE);
                        break;
                    case PLAYER1:
                        circle.setFill(Color.RED);
                        break;
                    case PLAYER2:
                        circle.setFill(Color.BLUE);
                        break;
                }
            }
        }
    }

    /**
     * Convert the base spots ('X', 'O', and ' ') to objects.
     * @param baseSpots char[][]
     * @return char[][]
     */
    private char[][] baseSpotsToObjects(char[][] baseSpots){
        for(int row = 0; row < 6; row++){
            for(int col = 0; col < 7; col++){
                switch (baseSpots[row][col]){
                    case ' ':
                        spots[row][col].value = EMPTY;
                        break;
                    case 'X':
                        spots[row][col].value = PLAYER1;
                        break;
                    case 'O':
                        spots[row][col].value = PLAYER2;
                        break;
                }
            }
        }

        return baseSpots;
    }

    /**
     * Restarts the game board so that the user can play again at any time.
     * @param mouseEvent MouseEvent
     * @throws Exception if something goes wrong while loading
     */
    public void restart(MouseEvent mouseEvent) throws Exception {
        // Reset the state variables for this class.
        game = new Connect4();
        setSpots();
        currentPlayer = PLAYER1;
        gameOver = false;
        winner = "Draw";
        computerThinking = false;

        // Switch scene back to the first one.
        Stage window = (Stage) ((Node)mouseEvent.getSource()).getScene().getWindow();
        Parent rootGame = FXMLLoader.load(getClass().getResource("settings.fxml"));
        window.setScene(new Scene(rootGame, 600, 400));
    }

    /**
     * Handles hover effect for the spots.
     * @param mouseEvent MouseEvent
     */
    public void mouseEntered(MouseEvent mouseEvent) {
        // TODO: Add hover effect.
        System.out.println("Hovering over spot.");
    }
}

/**
 * Spot objects hold information about each spot on the game board.
 * @author Spencer Brown
 * @version 1.0.0
 */
class Spot {
    /** The Circle object. */
    Circle circle;
    /** The class id of the spot. */
    String id;
    /** The value of the spot. */
    CheckerValue value;

    /**
     * Constructor to create Spot object.
     * @param circle Circle
     * @param id String
     */
    Spot(Circle circle, String id) {
        this.circle = circle;
        this.id = id;

        // Default value of Spot is EMPTY.
        this.value = EMPTY;
    }
}

/** Package global enum for the type of values a checker (spot) can have. */
enum CheckerValue {
    EMPTY, PLAYER1, PLAYER2;
}