FROM openjdk:8
COPY ./src /usr/src/myapp
WORKDIR /usr/src/myapp
RUN javac core/Connect4Server.java
CMD ["java", "core.Connect4Server"]