# SER216_Connect4
Connect4 Game that can be played in the terminal or in a GUI.

## Instructions for testing.
Please keep the following file structure.
ui package:
* Connect4GUI.java
* Connect4TextConsole.java (main() method resides here)
* Controller.java
* game.fxml
* settings.fxml

core package:
* Connect4
* Connect4ComputerPlayer

## Instructions for playing the game.
Please press on any empty spot to place a checker. At any time the game can be restarted by pressing the Restart Button
at the end of the window.